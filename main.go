package main

import (
	"fmt"
	"log"
	"net"
	"os"
	"strconv"
	"strings"
	"time"
	"sort"

	"github.com/therecipe/qt/core"
	"github.com/therecipe/qt/quick"
	"github.com/therecipe/qt/widgets"
)

const (
	Sender = int(core.Qt__UserRole) + 1<<iota
	Sent
	Id
	Text
)

type RecvMsg struct {
	Sender string
	Sent time.Time
	Id int32
	Text string
}

type MessageModel struct {
	core.QAbstractListModel

	_ map[int]*core.QByteArray `property:"roles"`
	_ []*RecvMsg `property:"messages"`

	_ func() `constructor:"init"`

	_ func(*RecvMsg) `slot:"addMsg"`
}

func (m *MessageModel) init() {
	m.SetRoles(map[int]*core.QByteArray{
		Sender: core.NewQByteArray2("sender", -1),
		Sent:  core.NewQByteArray2("sent", -1),
		Id:  core.NewQByteArray2("id", -1),
		Text:  core.NewQByteArray2("text", -1),
	})

	m.ConnectData(m.data)
	m.ConnectRowCount(m.rowCount)
	m.ConnectColumnCount(m.columnCount)
	m.ConnectRoleNames(m.roleNames)
	m.ConnectAddMsg(m.addMsg)
}

func (m *MessageModel) columnCount(parent *core.QModelIndex) int {
	return 1
}

func (m *MessageModel) roleNames() map[int]*core.QByteArray {
	return m.Roles()
}

func (m *MessageModel) addMsg(msg *RecvMsg) {
	idx := sort.Search(len(m.Messages()), func (ii int) bool {
		sent := m.Messages()[ii].Sent

		return sent.After(msg.Sent) || sent.Equal(msg.Sent)
	})

	log.Printf("Inserting msg %d at index %d", msg.Id, idx)

	m.BeginInsertRows(core.NewQModelIndex(), idx, idx)

	msgs := m.Messages()
	msgs = append(msgs, nil)
	copy(msgs[idx+1:], msgs[idx:])
	msgs[idx] = msg

	m.SetMessages(msgs)
	m.EndInsertRows()

	log.Printf("m.Messages() == %+v", m.Messages())
}

func (m *MessageModel) rowCount(parent *core.QModelIndex) int {
	return len(m.Messages())
}

func (m *MessageModel) data(index *core.QModelIndex, role int) *core.QVariant {
	if !index.IsValid() {
		return core.NewQVariant()
	}

	if index.Row() >= len(m.Messages()) {
		return core.NewQVariant()
	}

	var p = m.Messages()[index.Row()]

	switch role {
	case Sender:
		{
			return core.NewQVariant1(p.Sender)
		}

	case Id:
		{
			return core.NewQVariant1(p.Id)
		}

	case Text:
		{
			return core.NewQVariant1(p.Text)
		}

	case Sent:
		{
			return core.NewQVariant1(p.Sent.String())
		}

	default:
		{
			return core.NewQVariant()
		}
	}
}

type TestStruct struct {
	core.QObject

	_ func() `constructor:"init"`

	_ *MessageModel `property:"messages"`

	_ func(string) `slot:"send"`

	Node ChatNode
}

func (m *TestStruct) init() {
	m.ConnectSend(m.send)

	addr, err := (net.ResolveUDPAddr("udp", "239.202.145.171:1057"))
	if err != nil {
		log.Fatal(err)
	}

	conn, err := net.ListenMulticastUDP("udp", nil, addr)
	if err != nil {
		log.Fatal(err)
	}

	m.Node = ChatNode {
		peers: make(map[string]*Peer),
		sock: conn,
	}

	m.SetMessages(NewMessageModel(nil))

	go m.Node.sendHellos()
	go m.Node.receiver(m)
}

type ChatNode struct {
	peers map[string]*Peer
	sock *net.UDPConn
	sentMsgs []sentMsg
}

type sentMsg struct {
	sent time.Time
	text string
}

type Peer struct {
	ip *net.UDPAddr
	noRecv int64
	saidSent int64
}

func main() {
	core.QCoreApplication_SetAttribute(core.Qt__AA_EnableHighDpiScaling, true)

	app := widgets.NewQApplication(len(os.Args), os.Args)

	MessageModel_QRegisterMetaType()
	TestStruct_QRegisterMetaType()

	view := quick.NewQQuickView(nil)
	view.SetTitle("BarkBark")
	view.SetResizeMode(quick.QQuickView__SizeRootObjectToView)

	view.RootContext().SetContextProperty("testvar", NewTestStruct(nil))
	view.SetSource(core.NewQUrl3("qrc:/qml/Main.qml", 0))
	view.Show()

	app.Exec()
}

func (testvar *TestStruct) send(text string) {
	go testvar.Node.sendText(text)
}

func (this *ChatNode) sendText(text string) {
	now := time.Now()
	toSend := sentMsg {
		sent: now,
		text: text,
	}
	this.sentMsgs = append(this.sentMsgs, toSend)
	this.sendTextAt(len(this.sentMsgs) - 1)
}

func (this *ChatNode) sendTextAt(index int) {
	toSend := this.sentMsgs[index]
	msg := fmt.Sprintf("t|%d|%s|%s", index + 1, toSend.sent.Format(time.RFC3339), toSend.text)
	this.write(msg)
	log.Printf("Sending Text")
}

func (this *ChatNode) sendHellos() {
	timer := time.NewTicker(30*time.Second)

	for true {
		this.sendHello()
		<- timer.C
	}
}

func (this *ChatNode) sendHello() {
	now := time.Now().Format(time.RFC3339)
	msg := fmt.Sprintf("h|%d|%s", len(this.sentMsgs), now)
	this.write(msg)
	log.Printf("Sending hello")
}

func (this *ChatNode) sendEnumerate(addr *net.UDPAddr) {
	peer := this.peers[addr.String()]
	msg := fmt.Sprintf("e|%d", peer.noRecv + 1)
	this.writeTo(addr, msg)
	log.Printf("Sending enumeration")
}

func (this *ChatNode) write(msg string) {
	addr, err := (net.ResolveUDPAddr("udp", "239.202.145.171:1057"))
	if err != nil {
		log.Fatal(err)
	}
	this.writeTo(addr, msg)
}

func (this *ChatNode) writeTo(addr *net.UDPAddr, msg string) {
	buff := []byte(msg)
	log.Printf("Writing %d bytes to %s", len(buff), addr.String())
	_, _, err := this.sock.WriteMsgUDP(buff, nil, addr)
	if err != nil {
		log.Fatal(err)
	}
}

func (this *ChatNode) receiver(testvar *TestStruct) {
	var buffer[65000] byte
	var oob[0] byte

	for true {
		size, _, _,from, err:= this.sock.ReadMsgUDP(buffer[:], oob[:])
		if err != nil {
			log.Fatal(err)
		}

		if from == this.sock.LocalAddr() {
			continue
		}

		bytes := buffer[:size]
		msg := string (bytes)
		if strings.HasSuffix(msg, "\n") {
			msg = msg[:size-1]
		}

		log.Print("Got it chief %s", msg)

		switch msg[0] {
		case 't':
			handleText(testvar, from, msg)
		case 'h':
			handleHello(testvar, from, msg)
		case 'e':
			handleEnumerate(testvar, from, msg)
		}
	}
}

func handleText(testvar *TestStruct, ip *net.UDPAddr, msg string) {
	parts := strings.SplitN(msg, "|", 4)
	id, err := strconv.ParseInt(parts[1], 10, 64)
	if err != nil {
		log.Fatal(err)
	}

	sent, err := time.Parse(time.RFC3339, parts[2])
	if err != nil {
		log.Fatal(err)
	}

	recvMsg := testvar.Node.onTextMsg(ip, id, sent, parts[3])
	if recvMsg != nil {
		testvar.Messages().AddMsg(recvMsg)
	}
}

func handleHello(testvar *TestStruct, from *net.UDPAddr, msg string) {
	parts := strings.SplitN(msg, "|", 3)
	id, err := strconv.ParseInt(parts[1], 10, 64)
	if err != nil {
		log.Fatal(err)
	}

	_, err = time.Parse(time.RFC3339, parts[2])
	if err != nil {
		log.Fatal(err)
	}
	testvar.Node.onHelloMsg(from, id)
}

func handleEnumerate(testvar *TestStruct, from *net.UDPAddr, msg string) {
	log.Printf("%v Requested enumerate from %s", from, msg)
	
	parts := strings.SplitN(msg, "|", 2)
	id, err := strconv.ParseInt(parts[1], 10, 64)
	if err != nil {
		log.Fatal(err)
	}
	testvar.Node.onEnumerateMsg(from, id)
}

func (this *ChatNode) onEnumerateMsg(ip *net.UDPAddr, id int64) {
	for ii := int(id)-1; ii < len(this.sentMsgs); ii ++ {
		this.sendTextAt(ii)
	} 
}

func (this *ChatNode) onHelloMsg(ip *net.UDPAddr, saidSent int64) {
	if this.peers[ip.String()] == nil {
		this.peers[ip.String()] = &Peer {
			ip:ip,
			noRecv:0,
			saidSent:0,
		}
	}
	this.peers[ip.String()].saidSent = saidSent
	log.Printf("%v Has sent %d", ip, saidSent)
	noRecv := this.peers[ip.String()].noRecv
	
	if saidSent <= noRecv {
	return
	}

this.peers[ip.String()].saidSent	= saidSent
this.sendEnumerate(ip)
}

func (this *ChatNode) onTextMsg(ip *net.UDPAddr, id int64, sent time.Time, text string) *RecvMsg{
	if this.peers[ip.String()] == nil {
		log.Printf("New peer %v", ip)
		this.peers[ip.String()] = &Peer {
			ip:ip,
			noRecv:0,
			saidSent:0,
		}
	}

	noRecv := this.peers[ip.String()].noRecv
	log.Printf("%v Sent txt msg with id %d", ip, id)

	if id <= noRecv {
		// Discard Duplicate Message
	} else if id - noRecv == 1 {
		this.peers[ip.String()].noRecv += 1
		log.Printf("[%v]: %s", ip, text)

		return &RecvMsg {
			Text: text,
			Sent: sent,
			Id: int32(id),
			Sender: ip.String(),
		}
	} else {
		this.peers[ip.String()].saidSent	= id
		this.sendEnumerate(ip)
	}

	return nil
}
