package main

import (
	"fmt"
	"log"
	"net"
	"bufio"
	"os"
)

func main() {
	done := make(chan bool)

	go receiver(done)
	sender()
	<- done
	
}

func sender() {
	addr, err := (net.ResolveUDPAddr("udp", "239.202.145.171:1057"))
	if err != nil {
		log.Fatal(err)
	}

	conn, err := net.DialUDP("udp", nil, addr)
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	reader := bufio.NewReader(os.Stdin)
	
	fmt.Printf("%s", ">>> ")
	msg, err := reader.ReadString('\n')
	if err != nil {
		log.Fatal(err)
	}

	fmt.Fprintf(conn, "%s", msg)
	
}

func receiver(done chan bool) {
	addr, err := (net.ResolveUDPAddr("udp", "239.202.145.171:1057"))
	if err != nil {
		log.Fatal(err)
	}

	ln, err := net.ListenMulticastUDP("udp", nil, addr)
	if err != nil {
		log.Fatal(err)
	}

	reader := bufio.NewReader(ln)

	msg, err := reader.ReadString('\n')
	if err != nil {
		log.Fatal(err)
	}

	fmt.Print(msg)
	done <- true
}