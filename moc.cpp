

#define protected public
#define private public

#include "moc.h"
#include "_cgo_export.h"

#include <QAbstractItemModel>
#include <QAbstractListModel>
#include <QByteArray>
#include <QChildEvent>
#include <QEvent>
#include <QGraphicsObject>
#include <QGraphicsWidget>
#include <QHash>
#include <QLayout>
#include <QList>
#include <QMap>
#include <QMetaMethod>
#include <QMetaObject>
#include <QMimeData>
#include <QModelIndex>
#include <QObject>
#include <QOffscreenSurface>
#include <QPaintDeviceWindow>
#include <QPdfWriter>
#include <QPersistentModelIndex>
#include <QQuickItem>
#include <QSize>
#include <QString>
#include <QTimerEvent>
#include <QVariant>
#include <QVector>
#include <QWidget>
#include <QWindow>

#ifdef QT_QML_LIB
	#include <QQmlEngine>
#endif


typedef QMap<qint32, QByteArray> type378cdd;
class TestStructdb5825: public QObject
{
Q_OBJECT
Q_PROPERTY(MessageModeldb5825* messages READ messages WRITE setMessages NOTIFY messagesChanged)
public:
	TestStructdb5825(QObject *parent = Q_NULLPTR) : QObject(parent) {qRegisterMetaType<quintptr>("quintptr");TestStructdb5825_TestStructdb5825_QRegisterMetaType();TestStructdb5825_TestStructdb5825_QRegisterMetaTypes();callbackTestStructdb5825_Constructor(this);};
	MessageModeldb5825* messages() { return static_cast<MessageModeldb5825*>(callbackTestStructdb5825_Messages(this)); };
	void setMessages(MessageModeldb5825* messages) { callbackTestStructdb5825_SetMessages(this, messages); };
	void Signal_MessagesChanged(MessageModeldb5825* messages) { callbackTestStructdb5825_MessagesChanged(this, messages); };
	 ~TestStructdb5825() { callbackTestStructdb5825_DestroyTestStruct(this); };
	bool event(QEvent * e) { return callbackTestStructdb5825_Event(this, e) != 0; };
	bool eventFilter(QObject * watched, QEvent * event) { return callbackTestStructdb5825_EventFilter(this, watched, event) != 0; };
	void childEvent(QChildEvent * event) { callbackTestStructdb5825_ChildEvent(this, event); };
	void connectNotify(const QMetaMethod & sign) { callbackTestStructdb5825_ConnectNotify(this, const_cast<QMetaMethod*>(&sign)); };
	void customEvent(QEvent * event) { callbackTestStructdb5825_CustomEvent(this, event); };
	void deleteLater() { callbackTestStructdb5825_DeleteLater(this); };
	void Signal_Destroyed(QObject * obj) { callbackTestStructdb5825_Destroyed(this, obj); };
	void disconnectNotify(const QMetaMethod & sign) { callbackTestStructdb5825_DisconnectNotify(this, const_cast<QMetaMethod*>(&sign)); };
	void Signal_ObjectNameChanged(const QString & objectName) { QByteArray* taa2c4f = new QByteArray(objectName.toUtf8()); Moc_PackedString objectNamePacked = { const_cast<char*>(taa2c4f->prepend("WHITESPACE").constData()+10), taa2c4f->size()-10, taa2c4f };callbackTestStructdb5825_ObjectNameChanged(this, objectNamePacked); };
	void timerEvent(QTimerEvent * event) { callbackTestStructdb5825_TimerEvent(this, event); };
	MessageModeldb5825* messagesDefault() { return _messages; };
	void setMessagesDefault(MessageModeldb5825* p) { if (p != _messages) { _messages = p; messagesChanged(_messages); } };
signals:
	void messagesChanged(MessageModeldb5825* messages);
public slots:
	void send(QString v0) { QByteArray* tea1dd7 = new QByteArray(v0.toUtf8()); Moc_PackedString v0Packed = { const_cast<char*>(tea1dd7->prepend("WHITESPACE").constData()+10), tea1dd7->size()-10, tea1dd7 };callbackTestStructdb5825_Send(this, v0Packed); };
private:
	MessageModeldb5825* _messages;
};

Q_DECLARE_METATYPE(TestStructdb5825*)


void TestStructdb5825_TestStructdb5825_QRegisterMetaTypes() {
}

class MessageModeldb5825: public QAbstractListModel
{
Q_OBJECT
Q_PROPERTY(type378cdd roles READ roles WRITE setRoles NOTIFY rolesChanged)
Q_PROPERTY(quintptr messages READ messages WRITE setMessages NOTIFY messagesChanged)
public:
	MessageModeldb5825(QObject *parent = Q_NULLPTR) : QAbstractListModel(parent) {qRegisterMetaType<quintptr>("quintptr");MessageModeldb5825_MessageModeldb5825_QRegisterMetaType();MessageModeldb5825_MessageModeldb5825_QRegisterMetaTypes();callbackMessageModeldb5825_Constructor(this);};
	type378cdd roles() { return ({ QMap<qint32, QByteArray>* tmpP = static_cast<QMap<qint32, QByteArray>*>(callbackMessageModeldb5825_Roles(this)); QMap<qint32, QByteArray> tmpV = *tmpP; tmpP->~QMap(); free(tmpP); tmpV; }); };
	void setRoles(type378cdd roles) { callbackMessageModeldb5825_SetRoles(this, ({ QMap<qint32, QByteArray>* tmpValue037c88 = new QMap<qint32, QByteArray>(roles); Moc_PackedList { tmpValue037c88, tmpValue037c88->size() }; })); };
	void Signal_RolesChanged(type378cdd roles) { callbackMessageModeldb5825_RolesChanged(this, ({ QMap<qint32, QByteArray>* tmpValue037c88 = new QMap<qint32, QByteArray>(roles); Moc_PackedList { tmpValue037c88, tmpValue037c88->size() }; })); };
	quintptr messages() { return callbackMessageModeldb5825_Messages(this); };
	void setMessages(quintptr messages) { callbackMessageModeldb5825_SetMessages(this, messages); };
	void Signal_MessagesChanged(quintptr messages) { callbackMessageModeldb5825_MessagesChanged(this, messages); };
	bool dropMimeData(const QMimeData * data, Qt::DropAction action, int row, int column, const QModelIndex & parent) { return callbackMessageModeldb5825_DropMimeData(this, const_cast<QMimeData*>(data), action, row, column, const_cast<QModelIndex*>(&parent)) != 0; };
	QModelIndex index(int row, int column, const QModelIndex & parent) const { return *static_cast<QModelIndex*>(callbackMessageModeldb5825_Index(const_cast<void*>(static_cast<const void*>(this)), row, column, const_cast<QModelIndex*>(&parent))); };
	QModelIndex sibling(int row, int column, const QModelIndex & idx) const { return *static_cast<QModelIndex*>(callbackMessageModeldb5825_Sibling(const_cast<void*>(static_cast<const void*>(this)), row, column, const_cast<QModelIndex*>(&idx))); };
	Qt::ItemFlags flags(const QModelIndex & index) const { return static_cast<Qt::ItemFlag>(callbackMessageModeldb5825_Flags(const_cast<void*>(static_cast<const void*>(this)), const_cast<QModelIndex*>(&index))); };
	bool insertColumns(int column, int count, const QModelIndex & parent) { return callbackMessageModeldb5825_InsertColumns(this, column, count, const_cast<QModelIndex*>(&parent)) != 0; };
	bool insertRows(int row, int count, const QModelIndex & parent) { return callbackMessageModeldb5825_InsertRows(this, row, count, const_cast<QModelIndex*>(&parent)) != 0; };
	bool moveColumns(const QModelIndex & sourceParent, int sourceColumn, int count, const QModelIndex & destinationParent, int destinationChild) { return callbackMessageModeldb5825_MoveColumns(this, const_cast<QModelIndex*>(&sourceParent), sourceColumn, count, const_cast<QModelIndex*>(&destinationParent), destinationChild) != 0; };
	bool moveRows(const QModelIndex & sourceParent, int sourceRow, int count, const QModelIndex & destinationParent, int destinationChild) { return callbackMessageModeldb5825_MoveRows(this, const_cast<QModelIndex*>(&sourceParent), sourceRow, count, const_cast<QModelIndex*>(&destinationParent), destinationChild) != 0; };
	bool removeColumns(int column, int count, const QModelIndex & parent) { return callbackMessageModeldb5825_RemoveColumns(this, column, count, const_cast<QModelIndex*>(&parent)) != 0; };
	bool removeRows(int row, int count, const QModelIndex & parent) { return callbackMessageModeldb5825_RemoveRows(this, row, count, const_cast<QModelIndex*>(&parent)) != 0; };
	bool setData(const QModelIndex & index, const QVariant & value, int role) { return callbackMessageModeldb5825_SetData(this, const_cast<QModelIndex*>(&index), const_cast<QVariant*>(&value), role) != 0; };
	bool setHeaderData(int section, Qt::Orientation orientation, const QVariant & value, int role) { return callbackMessageModeldb5825_SetHeaderData(this, section, orientation, const_cast<QVariant*>(&value), role) != 0; };
	bool setItemData(const QModelIndex & index, const QMap<int, QVariant> & roles) { return callbackMessageModeldb5825_SetItemData(this, const_cast<QModelIndex*>(&index), ({ QMap<int, QVariant>* tmpValue037c88 = new QMap<int, QVariant>(roles); Moc_PackedList { tmpValue037c88, tmpValue037c88->size() }; })) != 0; };
	bool submit() { return callbackMessageModeldb5825_Submit(this) != 0; };
	void Signal_ColumnsAboutToBeInserted(const QModelIndex & parent, int first, int last) { callbackMessageModeldb5825_ColumnsAboutToBeInserted(this, const_cast<QModelIndex*>(&parent), first, last); };
	void Signal_ColumnsAboutToBeMoved(const QModelIndex & sourceParent, int sourceStart, int sourceEnd, const QModelIndex & destinationParent, int destinationColumn) { callbackMessageModeldb5825_ColumnsAboutToBeMoved(this, const_cast<QModelIndex*>(&sourceParent), sourceStart, sourceEnd, const_cast<QModelIndex*>(&destinationParent), destinationColumn); };
	void Signal_ColumnsAboutToBeRemoved(const QModelIndex & parent, int first, int last) { callbackMessageModeldb5825_ColumnsAboutToBeRemoved(this, const_cast<QModelIndex*>(&parent), first, last); };
	void Signal_ColumnsInserted(const QModelIndex & parent, int first, int last) { callbackMessageModeldb5825_ColumnsInserted(this, const_cast<QModelIndex*>(&parent), first, last); };
	void Signal_ColumnsMoved(const QModelIndex & parent, int start, int end, const QModelIndex & destination, int column) { callbackMessageModeldb5825_ColumnsMoved(this, const_cast<QModelIndex*>(&parent), start, end, const_cast<QModelIndex*>(&destination), column); };
	void Signal_ColumnsRemoved(const QModelIndex & parent, int first, int last) { callbackMessageModeldb5825_ColumnsRemoved(this, const_cast<QModelIndex*>(&parent), first, last); };
	void Signal_DataChanged(const QModelIndex & topLeft, const QModelIndex & bottomRight, const QVector<int> & roles) { callbackMessageModeldb5825_DataChanged(this, const_cast<QModelIndex*>(&topLeft), const_cast<QModelIndex*>(&bottomRight), ({ QVector<int>* tmpValue037c88 = new QVector<int>(roles); Moc_PackedList { tmpValue037c88, tmpValue037c88->size() }; })); };
	void fetchMore(const QModelIndex & parent) { callbackMessageModeldb5825_FetchMore(this, const_cast<QModelIndex*>(&parent)); };
	void Signal_HeaderDataChanged(Qt::Orientation orientation, int first, int last) { callbackMessageModeldb5825_HeaderDataChanged(this, orientation, first, last); };
	void Signal_LayoutAboutToBeChanged(const QList<QPersistentModelIndex> & parents, QAbstractItemModel::LayoutChangeHint hint) { callbackMessageModeldb5825_LayoutAboutToBeChanged(this, ({ QList<QPersistentModelIndex>* tmpValuea664f1 = new QList<QPersistentModelIndex>(parents); Moc_PackedList { tmpValuea664f1, tmpValuea664f1->size() }; }), hint); };
	void Signal_LayoutChanged(const QList<QPersistentModelIndex> & parents, QAbstractItemModel::LayoutChangeHint hint) { callbackMessageModeldb5825_LayoutChanged(this, ({ QList<QPersistentModelIndex>* tmpValuea664f1 = new QList<QPersistentModelIndex>(parents); Moc_PackedList { tmpValuea664f1, tmpValuea664f1->size() }; }), hint); };
	void Signal_ModelAboutToBeReset() { callbackMessageModeldb5825_ModelAboutToBeReset(this); };
	void Signal_ModelReset() { callbackMessageModeldb5825_ModelReset(this); };
	void resetInternalData() { callbackMessageModeldb5825_ResetInternalData(this); };
	void revert() { callbackMessageModeldb5825_Revert(this); };
	void Signal_RowsAboutToBeInserted(const QModelIndex & parent, int start, int end) { callbackMessageModeldb5825_RowsAboutToBeInserted(this, const_cast<QModelIndex*>(&parent), start, end); };
	void Signal_RowsAboutToBeMoved(const QModelIndex & sourceParent, int sourceStart, int sourceEnd, const QModelIndex & destinationParent, int destinationRow) { callbackMessageModeldb5825_RowsAboutToBeMoved(this, const_cast<QModelIndex*>(&sourceParent), sourceStart, sourceEnd, const_cast<QModelIndex*>(&destinationParent), destinationRow); };
	void Signal_RowsAboutToBeRemoved(const QModelIndex & parent, int first, int last) { callbackMessageModeldb5825_RowsAboutToBeRemoved(this, const_cast<QModelIndex*>(&parent), first, last); };
	void Signal_RowsInserted(const QModelIndex & parent, int first, int last) { callbackMessageModeldb5825_RowsInserted(this, const_cast<QModelIndex*>(&parent), first, last); };
	void Signal_RowsMoved(const QModelIndex & parent, int start, int end, const QModelIndex & destination, int row) { callbackMessageModeldb5825_RowsMoved(this, const_cast<QModelIndex*>(&parent), start, end, const_cast<QModelIndex*>(&destination), row); };
	void Signal_RowsRemoved(const QModelIndex & parent, int first, int last) { callbackMessageModeldb5825_RowsRemoved(this, const_cast<QModelIndex*>(&parent), first, last); };
	void sort(int column, Qt::SortOrder order) { callbackMessageModeldb5825_Sort(this, column, order); };
	QHash<int, QByteArray> roleNames() const { return ({ QHash<int, QByteArray>* tmpP = static_cast<QHash<int, QByteArray>*>(callbackMessageModeldb5825_RoleNames(const_cast<void*>(static_cast<const void*>(this)))); QHash<int, QByteArray> tmpV = *tmpP; tmpP->~QHash(); free(tmpP); tmpV; }); };
	QMap<int, QVariant> itemData(const QModelIndex & index) const { return ({ QMap<int, QVariant>* tmpP = static_cast<QMap<int, QVariant>*>(callbackMessageModeldb5825_ItemData(const_cast<void*>(static_cast<const void*>(this)), const_cast<QModelIndex*>(&index))); QMap<int, QVariant> tmpV = *tmpP; tmpP->~QMap(); free(tmpP); tmpV; }); };
	QMimeData * mimeData(const QModelIndexList & indexes) const { return static_cast<QMimeData*>(callbackMessageModeldb5825_MimeData(const_cast<void*>(static_cast<const void*>(this)), ({ QList<QModelIndex>* tmpValuee0adf2 = new QList<QModelIndex>(indexes); Moc_PackedList { tmpValuee0adf2, tmpValuee0adf2->size() }; }))); };
	QModelIndex buddy(const QModelIndex & index) const { return *static_cast<QModelIndex*>(callbackMessageModeldb5825_Buddy(const_cast<void*>(static_cast<const void*>(this)), const_cast<QModelIndex*>(&index))); };
	QModelIndex parent(const QModelIndex & index) const { return *static_cast<QModelIndex*>(callbackMessageModeldb5825_Parent(const_cast<void*>(static_cast<const void*>(this)), const_cast<QModelIndex*>(&index))); };
	QList<QModelIndex> match(const QModelIndex & start, int role, const QVariant & value, int hits, Qt::MatchFlags flags) const { return ({ QList<QModelIndex>* tmpP = static_cast<QList<QModelIndex>*>(callbackMessageModeldb5825_Match(const_cast<void*>(static_cast<const void*>(this)), const_cast<QModelIndex*>(&start), role, const_cast<QVariant*>(&value), hits, flags)); QList<QModelIndex> tmpV = *tmpP; tmpP->~QList(); free(tmpP); tmpV; }); };
	QSize span(const QModelIndex & index) const { return *static_cast<QSize*>(callbackMessageModeldb5825_Span(const_cast<void*>(static_cast<const void*>(this)), const_cast<QModelIndex*>(&index))); };
	QStringList mimeTypes() const { return ({ Moc_PackedString tempVal = callbackMessageModeldb5825_MimeTypes(const_cast<void*>(static_cast<const void*>(this))); QStringList ret = QString::fromUtf8(tempVal.data, tempVal.len).split("¡¦!", QString::SkipEmptyParts); free(tempVal.data); ret; }); };
	QVariant data(const QModelIndex & index, int role) const { return *static_cast<QVariant*>(callbackMessageModeldb5825_Data(const_cast<void*>(static_cast<const void*>(this)), const_cast<QModelIndex*>(&index), role)); };
	QVariant headerData(int section, Qt::Orientation orientation, int role) const { return *static_cast<QVariant*>(callbackMessageModeldb5825_HeaderData(const_cast<void*>(static_cast<const void*>(this)), section, orientation, role)); };
	Qt::DropActions supportedDragActions() const { return static_cast<Qt::DropAction>(callbackMessageModeldb5825_SupportedDragActions(const_cast<void*>(static_cast<const void*>(this)))); };
	Qt::DropActions supportedDropActions() const { return static_cast<Qt::DropAction>(callbackMessageModeldb5825_SupportedDropActions(const_cast<void*>(static_cast<const void*>(this)))); };
	bool canDropMimeData(const QMimeData * data, Qt::DropAction action, int row, int column, const QModelIndex & parent) const { return callbackMessageModeldb5825_CanDropMimeData(const_cast<void*>(static_cast<const void*>(this)), const_cast<QMimeData*>(data), action, row, column, const_cast<QModelIndex*>(&parent)) != 0; };
	bool canFetchMore(const QModelIndex & parent) const { return callbackMessageModeldb5825_CanFetchMore(const_cast<void*>(static_cast<const void*>(this)), const_cast<QModelIndex*>(&parent)) != 0; };
	bool hasChildren(const QModelIndex & parent) const { return callbackMessageModeldb5825_HasChildren(const_cast<void*>(static_cast<const void*>(this)), const_cast<QModelIndex*>(&parent)) != 0; };
	int columnCount(const QModelIndex & parent) const { return callbackMessageModeldb5825_ColumnCount(const_cast<void*>(static_cast<const void*>(this)), const_cast<QModelIndex*>(&parent)); };
	int rowCount(const QModelIndex & parent) const { return callbackMessageModeldb5825_RowCount(const_cast<void*>(static_cast<const void*>(this)), const_cast<QModelIndex*>(&parent)); };
	bool event(QEvent * e) { return callbackMessageModeldb5825_Event(this, e) != 0; };
	bool eventFilter(QObject * watched, QEvent * event) { return callbackMessageModeldb5825_EventFilter(this, watched, event) != 0; };
	void childEvent(QChildEvent * event) { callbackMessageModeldb5825_ChildEvent(this, event); };
	void connectNotify(const QMetaMethod & sign) { callbackMessageModeldb5825_ConnectNotify(this, const_cast<QMetaMethod*>(&sign)); };
	void customEvent(QEvent * event) { callbackMessageModeldb5825_CustomEvent(this, event); };
	void deleteLater() { callbackMessageModeldb5825_DeleteLater(this); };
	void Signal_Destroyed(QObject * obj) { callbackMessageModeldb5825_Destroyed(this, obj); };
	void disconnectNotify(const QMetaMethod & sign) { callbackMessageModeldb5825_DisconnectNotify(this, const_cast<QMetaMethod*>(&sign)); };
	void Signal_ObjectNameChanged(const QString & objectName) { QByteArray* taa2c4f = new QByteArray(objectName.toUtf8()); Moc_PackedString objectNamePacked = { const_cast<char*>(taa2c4f->prepend("WHITESPACE").constData()+10), taa2c4f->size()-10, taa2c4f };callbackMessageModeldb5825_ObjectNameChanged(this, objectNamePacked); };
	void timerEvent(QTimerEvent * event) { callbackMessageModeldb5825_TimerEvent(this, event); };
	type378cdd rolesDefault() { return _roles; };
	void setRolesDefault(type378cdd p) { if (p != _roles) { _roles = p; rolesChanged(_roles); } };
	quintptr messagesDefault() { return _messages; };
	void setMessagesDefault(quintptr p) { if (p != _messages) { _messages = p; messagesChanged(_messages); } };
signals:
	void rolesChanged(type378cdd roles);
	void messagesChanged(quintptr messages);
public slots:
	void addMsg(quintptr v0) { callbackMessageModeldb5825_AddMsg(this, v0); };
private:
	type378cdd _roles;
	quintptr _messages;
};

Q_DECLARE_METATYPE(MessageModeldb5825*)


void MessageModeldb5825_MessageModeldb5825_QRegisterMetaTypes() {
	qRegisterMetaType<type378cdd>("type378cdd");
}

void MessageModeldb5825_AddMsg(void* ptr, uintptr_t v0)
{
	QMetaObject::invokeMethod(static_cast<MessageModeldb5825*>(ptr), "addMsg", Q_ARG(quintptr, v0));
}

struct Moc_PackedList MessageModeldb5825_Roles(void* ptr)
{
	return ({ QMap<qint32, QByteArray>* tmpValue66fa9c = new QMap<qint32, QByteArray>(static_cast<MessageModeldb5825*>(ptr)->roles()); Moc_PackedList { tmpValue66fa9c, tmpValue66fa9c->size() }; });
}

struct Moc_PackedList MessageModeldb5825_RolesDefault(void* ptr)
{
	return ({ QMap<qint32, QByteArray>* tmpValue4a7c7b = new QMap<qint32, QByteArray>(static_cast<MessageModeldb5825*>(ptr)->rolesDefault()); Moc_PackedList { tmpValue4a7c7b, tmpValue4a7c7b->size() }; });
}

void MessageModeldb5825_SetRoles(void* ptr, void* roles)
{
	static_cast<MessageModeldb5825*>(ptr)->setRoles(({ QMap<qint32, QByteArray>* tmpP = static_cast<QMap<qint32, QByteArray>*>(roles); QMap<qint32, QByteArray> tmpV = *tmpP; tmpP->~QMap(); free(tmpP); tmpV; }));
}

void MessageModeldb5825_SetRolesDefault(void* ptr, void* roles)
{
	static_cast<MessageModeldb5825*>(ptr)->setRolesDefault(({ QMap<qint32, QByteArray>* tmpP = static_cast<QMap<qint32, QByteArray>*>(roles); QMap<qint32, QByteArray> tmpV = *tmpP; tmpP->~QMap(); free(tmpP); tmpV; }));
}

void MessageModeldb5825_ConnectRolesChanged(void* ptr, long long t)
{
	QObject::connect(static_cast<MessageModeldb5825*>(ptr), static_cast<void (MessageModeldb5825::*)(QMap<qint32, QByteArray>)>(&MessageModeldb5825::rolesChanged), static_cast<MessageModeldb5825*>(ptr), static_cast<void (MessageModeldb5825::*)(QMap<qint32, QByteArray>)>(&MessageModeldb5825::Signal_RolesChanged), static_cast<Qt::ConnectionType>(t));
}

void MessageModeldb5825_DisconnectRolesChanged(void* ptr)
{
	QObject::disconnect(static_cast<MessageModeldb5825*>(ptr), static_cast<void (MessageModeldb5825::*)(QMap<qint32, QByteArray>)>(&MessageModeldb5825::rolesChanged), static_cast<MessageModeldb5825*>(ptr), static_cast<void (MessageModeldb5825::*)(QMap<qint32, QByteArray>)>(&MessageModeldb5825::Signal_RolesChanged));
}

void MessageModeldb5825_RolesChanged(void* ptr, void* roles)
{
	static_cast<MessageModeldb5825*>(ptr)->rolesChanged(({ QMap<qint32, QByteArray>* tmpP = static_cast<QMap<qint32, QByteArray>*>(roles); QMap<qint32, QByteArray> tmpV = *tmpP; tmpP->~QMap(); free(tmpP); tmpV; }));
}

uintptr_t MessageModeldb5825_Messages(void* ptr)
{
	return static_cast<MessageModeldb5825*>(ptr)->messages();
}

uintptr_t MessageModeldb5825_MessagesDefault(void* ptr)
{
	return static_cast<MessageModeldb5825*>(ptr)->messagesDefault();
}

void MessageModeldb5825_SetMessages(void* ptr, uintptr_t messages)
{
	static_cast<MessageModeldb5825*>(ptr)->setMessages(messages);
}

void MessageModeldb5825_SetMessagesDefault(void* ptr, uintptr_t messages)
{
	static_cast<MessageModeldb5825*>(ptr)->setMessagesDefault(messages);
}

void MessageModeldb5825_ConnectMessagesChanged(void* ptr, long long t)
{
	QObject::connect(static_cast<MessageModeldb5825*>(ptr), static_cast<void (MessageModeldb5825::*)(quintptr)>(&MessageModeldb5825::messagesChanged), static_cast<MessageModeldb5825*>(ptr), static_cast<void (MessageModeldb5825::*)(quintptr)>(&MessageModeldb5825::Signal_MessagesChanged), static_cast<Qt::ConnectionType>(t));
}

void MessageModeldb5825_DisconnectMessagesChanged(void* ptr)
{
	QObject::disconnect(static_cast<MessageModeldb5825*>(ptr), static_cast<void (MessageModeldb5825::*)(quintptr)>(&MessageModeldb5825::messagesChanged), static_cast<MessageModeldb5825*>(ptr), static_cast<void (MessageModeldb5825::*)(quintptr)>(&MessageModeldb5825::Signal_MessagesChanged));
}

void MessageModeldb5825_MessagesChanged(void* ptr, uintptr_t messages)
{
	static_cast<MessageModeldb5825*>(ptr)->messagesChanged(messages);
}

int MessageModeldb5825_MessageModeldb5825_QRegisterMetaType()
{
	return qRegisterMetaType<MessageModeldb5825*>();
}

int MessageModeldb5825_MessageModeldb5825_QRegisterMetaType2(char* typeName)
{
	return qRegisterMetaType<MessageModeldb5825*>(const_cast<const char*>(typeName));
}

int MessageModeldb5825_MessageModeldb5825_QmlRegisterType()
{
#ifdef QT_QML_LIB
	return qmlRegisterType<MessageModeldb5825>();
#else
	return 0;
#endif
}

int MessageModeldb5825_MessageModeldb5825_QmlRegisterType2(char* uri, int versionMajor, int versionMinor, char* qmlName)
{
#ifdef QT_QML_LIB
	return qmlRegisterType<MessageModeldb5825>(const_cast<const char*>(uri), versionMajor, versionMinor, const_cast<const char*>(qmlName));
#else
	return 0;
#endif
}

int MessageModeldb5825_MessageModeldb5825_QmlRegisterUncreatableType(char* uri, int versionMajor, int versionMinor, char* qmlName, struct Moc_PackedString message)
{
#ifdef QT_QML_LIB
	return qmlRegisterUncreatableType<MessageModeldb5825>(const_cast<const char*>(uri), versionMajor, versionMinor, const_cast<const char*>(qmlName), QString::fromUtf8(message.data, message.len));
#else
	return 0;
#endif
}

int MessageModeldb5825_____setItemData_roles_keyList_atList(void* ptr, int i)
{
	return ({int tmp = static_cast<QList<int>*>(ptr)->at(i); if (i == static_cast<QList<int>*>(ptr)->size()-1) { static_cast<QList<int>*>(ptr)->~QList(); free(ptr); }; tmp; });
}

void MessageModeldb5825_____setItemData_roles_keyList_setList(void* ptr, int i)
{
	static_cast<QList<int>*>(ptr)->append(i);
}

void* MessageModeldb5825_____setItemData_roles_keyList_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<int>();
}

int MessageModeldb5825_____roleNames_keyList_atList(void* ptr, int i)
{
	return ({int tmp = static_cast<QList<int>*>(ptr)->at(i); if (i == static_cast<QList<int>*>(ptr)->size()-1) { static_cast<QList<int>*>(ptr)->~QList(); free(ptr); }; tmp; });
}

void MessageModeldb5825_____roleNames_keyList_setList(void* ptr, int i)
{
	static_cast<QList<int>*>(ptr)->append(i);
}

void* MessageModeldb5825_____roleNames_keyList_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<int>();
}

int MessageModeldb5825_____itemData_keyList_atList(void* ptr, int i)
{
	return ({int tmp = static_cast<QList<int>*>(ptr)->at(i); if (i == static_cast<QList<int>*>(ptr)->size()-1) { static_cast<QList<int>*>(ptr)->~QList(); free(ptr); }; tmp; });
}

void MessageModeldb5825_____itemData_keyList_setList(void* ptr, int i)
{
	static_cast<QList<int>*>(ptr)->append(i);
}

void* MessageModeldb5825_____itemData_keyList_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<int>();
}

void* MessageModeldb5825___setItemData_roles_atList(void* ptr, int v, int i)
{
	return new QVariant(({ QVariant tmp = static_cast<QMap<int, QVariant>*>(ptr)->value(v); if (i == static_cast<QMap<int, QVariant>*>(ptr)->size()-1) { static_cast<QMap<int, QVariant>*>(ptr)->~QMap(); free(ptr); }; tmp; }));
}

void MessageModeldb5825___setItemData_roles_setList(void* ptr, int key, void* i)
{
	static_cast<QMap<int, QVariant>*>(ptr)->insert(key, *static_cast<QVariant*>(i));
}

void* MessageModeldb5825___setItemData_roles_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QMap<int, QVariant>();
}

struct Moc_PackedList MessageModeldb5825___setItemData_roles_keyList(void* ptr)
{
	return ({ QList<int>* tmpValue249128 = new QList<int>(static_cast<QMap<int, QVariant>*>(ptr)->keys()); Moc_PackedList { tmpValue249128, tmpValue249128->size() }; });
}

void* MessageModeldb5825___changePersistentIndexList_from_atList(void* ptr, int i)
{
	return new QModelIndex(({QModelIndex tmp = static_cast<QList<QModelIndex>*>(ptr)->at(i); if (i == static_cast<QList<QModelIndex>*>(ptr)->size()-1) { static_cast<QList<QModelIndex>*>(ptr)->~QList(); free(ptr); }; tmp; }));
}

void MessageModeldb5825___changePersistentIndexList_from_setList(void* ptr, void* i)
{
	static_cast<QList<QModelIndex>*>(ptr)->append(*static_cast<QModelIndex*>(i));
}

void* MessageModeldb5825___changePersistentIndexList_from_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QModelIndex>();
}

void* MessageModeldb5825___changePersistentIndexList_to_atList(void* ptr, int i)
{
	return new QModelIndex(({QModelIndex tmp = static_cast<QList<QModelIndex>*>(ptr)->at(i); if (i == static_cast<QList<QModelIndex>*>(ptr)->size()-1) { static_cast<QList<QModelIndex>*>(ptr)->~QList(); free(ptr); }; tmp; }));
}

void MessageModeldb5825___changePersistentIndexList_to_setList(void* ptr, void* i)
{
	static_cast<QList<QModelIndex>*>(ptr)->append(*static_cast<QModelIndex*>(i));
}

void* MessageModeldb5825___changePersistentIndexList_to_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QModelIndex>();
}

int MessageModeldb5825___dataChanged_roles_atList(void* ptr, int i)
{
	return ({int tmp = static_cast<QVector<int>*>(ptr)->at(i); if (i == static_cast<QVector<int>*>(ptr)->size()-1) { static_cast<QVector<int>*>(ptr)->~QVector(); free(ptr); }; tmp; });
}

void MessageModeldb5825___dataChanged_roles_setList(void* ptr, int i)
{
	static_cast<QVector<int>*>(ptr)->append(i);
}

void* MessageModeldb5825___dataChanged_roles_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QVector<int>();
}

void* MessageModeldb5825___layoutAboutToBeChanged_parents_atList(void* ptr, int i)
{
	return new QPersistentModelIndex(({QPersistentModelIndex tmp = static_cast<QList<QPersistentModelIndex>*>(ptr)->at(i); if (i == static_cast<QList<QPersistentModelIndex>*>(ptr)->size()-1) { static_cast<QList<QPersistentModelIndex>*>(ptr)->~QList(); free(ptr); }; tmp; }));
}

void MessageModeldb5825___layoutAboutToBeChanged_parents_setList(void* ptr, void* i)
{
	static_cast<QList<QPersistentModelIndex>*>(ptr)->append(*static_cast<QPersistentModelIndex*>(i));
}

void* MessageModeldb5825___layoutAboutToBeChanged_parents_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QPersistentModelIndex>();
}

void* MessageModeldb5825___layoutChanged_parents_atList(void* ptr, int i)
{
	return new QPersistentModelIndex(({QPersistentModelIndex tmp = static_cast<QList<QPersistentModelIndex>*>(ptr)->at(i); if (i == static_cast<QList<QPersistentModelIndex>*>(ptr)->size()-1) { static_cast<QList<QPersistentModelIndex>*>(ptr)->~QList(); free(ptr); }; tmp; }));
}

void MessageModeldb5825___layoutChanged_parents_setList(void* ptr, void* i)
{
	static_cast<QList<QPersistentModelIndex>*>(ptr)->append(*static_cast<QPersistentModelIndex*>(i));
}

void* MessageModeldb5825___layoutChanged_parents_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QPersistentModelIndex>();
}

void* MessageModeldb5825___roleNames_atList(void* ptr, int v, int i)
{
	return new QByteArray(({ QByteArray tmp = static_cast<QHash<int, QByteArray>*>(ptr)->value(v); if (i == static_cast<QHash<int, QByteArray>*>(ptr)->size()-1) { static_cast<QHash<int, QByteArray>*>(ptr)->~QHash(); free(ptr); }; tmp; }));
}

void MessageModeldb5825___roleNames_setList(void* ptr, int key, void* i)
{
	static_cast<QHash<int, QByteArray>*>(ptr)->insert(key, *static_cast<QByteArray*>(i));
}

void* MessageModeldb5825___roleNames_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QHash<int, QByteArray>();
}

struct Moc_PackedList MessageModeldb5825___roleNames_keyList(void* ptr)
{
	return ({ QList<int>* tmpValue7fc3bb = new QList<int>(static_cast<QHash<int, QByteArray>*>(ptr)->keys()); Moc_PackedList { tmpValue7fc3bb, tmpValue7fc3bb->size() }; });
}

void* MessageModeldb5825___itemData_atList(void* ptr, int v, int i)
{
	return new QVariant(({ QVariant tmp = static_cast<QMap<int, QVariant>*>(ptr)->value(v); if (i == static_cast<QMap<int, QVariant>*>(ptr)->size()-1) { static_cast<QMap<int, QVariant>*>(ptr)->~QMap(); free(ptr); }; tmp; }));
}

void MessageModeldb5825___itemData_setList(void* ptr, int key, void* i)
{
	static_cast<QMap<int, QVariant>*>(ptr)->insert(key, *static_cast<QVariant*>(i));
}

void* MessageModeldb5825___itemData_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QMap<int, QVariant>();
}

struct Moc_PackedList MessageModeldb5825___itemData_keyList(void* ptr)
{
	return ({ QList<int>* tmpValue249128 = new QList<int>(static_cast<QMap<int, QVariant>*>(ptr)->keys()); Moc_PackedList { tmpValue249128, tmpValue249128->size() }; });
}

void* MessageModeldb5825___mimeData_indexes_atList(void* ptr, int i)
{
	return new QModelIndex(({QModelIndex tmp = static_cast<QList<QModelIndex>*>(ptr)->at(i); if (i == static_cast<QList<QModelIndex>*>(ptr)->size()-1) { static_cast<QList<QModelIndex>*>(ptr)->~QList(); free(ptr); }; tmp; }));
}

void MessageModeldb5825___mimeData_indexes_setList(void* ptr, void* i)
{
	static_cast<QList<QModelIndex>*>(ptr)->append(*static_cast<QModelIndex*>(i));
}

void* MessageModeldb5825___mimeData_indexes_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QModelIndex>();
}

void* MessageModeldb5825___match_atList(void* ptr, int i)
{
	return new QModelIndex(({QModelIndex tmp = static_cast<QList<QModelIndex>*>(ptr)->at(i); if (i == static_cast<QList<QModelIndex>*>(ptr)->size()-1) { static_cast<QList<QModelIndex>*>(ptr)->~QList(); free(ptr); }; tmp; }));
}

void MessageModeldb5825___match_setList(void* ptr, void* i)
{
	static_cast<QList<QModelIndex>*>(ptr)->append(*static_cast<QModelIndex*>(i));
}

void* MessageModeldb5825___match_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QModelIndex>();
}

void* MessageModeldb5825___persistentIndexList_atList(void* ptr, int i)
{
	return new QModelIndex(({QModelIndex tmp = static_cast<QList<QModelIndex>*>(ptr)->at(i); if (i == static_cast<QList<QModelIndex>*>(ptr)->size()-1) { static_cast<QList<QModelIndex>*>(ptr)->~QList(); free(ptr); }; tmp; }));
}

void MessageModeldb5825___persistentIndexList_setList(void* ptr, void* i)
{
	static_cast<QList<QModelIndex>*>(ptr)->append(*static_cast<QModelIndex*>(i));
}

void* MessageModeldb5825___persistentIndexList_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QModelIndex>();
}

int MessageModeldb5825_____doSetRoleNames_roleNames_keyList_atList(void* ptr, int i)
{
	return ({int tmp = static_cast<QList<int>*>(ptr)->at(i); if (i == static_cast<QList<int>*>(ptr)->size()-1) { static_cast<QList<int>*>(ptr)->~QList(); free(ptr); }; tmp; });
}

void MessageModeldb5825_____doSetRoleNames_roleNames_keyList_setList(void* ptr, int i)
{
	static_cast<QList<int>*>(ptr)->append(i);
}

void* MessageModeldb5825_____doSetRoleNames_roleNames_keyList_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<int>();
}

int MessageModeldb5825_____setRoleNames_roleNames_keyList_atList(void* ptr, int i)
{
	return ({int tmp = static_cast<QList<int>*>(ptr)->at(i); if (i == static_cast<QList<int>*>(ptr)->size()-1) { static_cast<QList<int>*>(ptr)->~QList(); free(ptr); }; tmp; });
}

void MessageModeldb5825_____setRoleNames_roleNames_keyList_setList(void* ptr, int i)
{
	static_cast<QList<int>*>(ptr)->append(i);
}

void* MessageModeldb5825_____setRoleNames_roleNames_keyList_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<int>();
}

void* MessageModeldb5825___dynamicPropertyNames_atList(void* ptr, int i)
{
	return new QByteArray(({QByteArray tmp = static_cast<QList<QByteArray>*>(ptr)->at(i); if (i == static_cast<QList<QByteArray>*>(ptr)->size()-1) { static_cast<QList<QByteArray>*>(ptr)->~QList(); free(ptr); }; tmp; }));
}

void MessageModeldb5825___dynamicPropertyNames_setList(void* ptr, void* i)
{
	static_cast<QList<QByteArray>*>(ptr)->append(*static_cast<QByteArray*>(i));
}

void* MessageModeldb5825___dynamicPropertyNames_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QByteArray>();
}

void* MessageModeldb5825___findChildren_atList2(void* ptr, int i)
{
	return ({QObject* tmp = static_cast<QList<QObject*>*>(ptr)->at(i); if (i == static_cast<QList<QObject*>*>(ptr)->size()-1) { static_cast<QList<QObject*>*>(ptr)->~QList(); free(ptr); }; tmp; });
}

void MessageModeldb5825___findChildren_setList2(void* ptr, void* i)
{
	static_cast<QList<QObject*>*>(ptr)->append(static_cast<QObject*>(i));
}

void* MessageModeldb5825___findChildren_newList2(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QObject*>();
}

void* MessageModeldb5825___findChildren_atList3(void* ptr, int i)
{
	return ({QObject* tmp = static_cast<QList<QObject*>*>(ptr)->at(i); if (i == static_cast<QList<QObject*>*>(ptr)->size()-1) { static_cast<QList<QObject*>*>(ptr)->~QList(); free(ptr); }; tmp; });
}

void MessageModeldb5825___findChildren_setList3(void* ptr, void* i)
{
	static_cast<QList<QObject*>*>(ptr)->append(static_cast<QObject*>(i));
}

void* MessageModeldb5825___findChildren_newList3(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QObject*>();
}

void* MessageModeldb5825___findChildren_atList(void* ptr, int i)
{
	return ({QObject* tmp = static_cast<QList<QObject*>*>(ptr)->at(i); if (i == static_cast<QList<QObject*>*>(ptr)->size()-1) { static_cast<QList<QObject*>*>(ptr)->~QList(); free(ptr); }; tmp; });
}

void MessageModeldb5825___findChildren_setList(void* ptr, void* i)
{
	static_cast<QList<QObject*>*>(ptr)->append(static_cast<QObject*>(i));
}

void* MessageModeldb5825___findChildren_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QObject*>();
}

void* MessageModeldb5825___children_atList(void* ptr, int i)
{
	return ({QObject * tmp = static_cast<QList<QObject *>*>(ptr)->at(i); if (i == static_cast<QList<QObject *>*>(ptr)->size()-1) { static_cast<QList<QObject *>*>(ptr)->~QList(); free(ptr); }; tmp; });
}

void MessageModeldb5825___children_setList(void* ptr, void* i)
{
	static_cast<QList<QObject *>*>(ptr)->append(static_cast<QObject*>(i));
}

void* MessageModeldb5825___children_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QObject *>();
}

void* MessageModeldb5825___roles_atList(void* ptr, int v, int i)
{
	return new QByteArray(({ QByteArray tmp = static_cast<QMap<qint32, QByteArray>*>(ptr)->value(v); if (i == static_cast<QMap<qint32, QByteArray>*>(ptr)->size()-1) { static_cast<QMap<qint32, QByteArray>*>(ptr)->~QMap(); free(ptr); }; tmp; }));
}

void MessageModeldb5825___roles_setList(void* ptr, int key, void* i)
{
	static_cast<QMap<qint32, QByteArray>*>(ptr)->insert(key, *static_cast<QByteArray*>(i));
}

void* MessageModeldb5825___roles_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QMap<qint32, QByteArray>();
}

struct Moc_PackedList MessageModeldb5825___roles_keyList(void* ptr)
{
	return ({ QList<qint32>* tmpValuecf92c1 = new QList<qint32>(static_cast<QMap<qint32, QByteArray>*>(ptr)->keys()); Moc_PackedList { tmpValuecf92c1, tmpValuecf92c1->size() }; });
}

void* MessageModeldb5825___setRoles_roles_atList(void* ptr, int v, int i)
{
	return new QByteArray(({ QByteArray tmp = static_cast<QMap<qint32, QByteArray>*>(ptr)->value(v); if (i == static_cast<QMap<qint32, QByteArray>*>(ptr)->size()-1) { static_cast<QMap<qint32, QByteArray>*>(ptr)->~QMap(); free(ptr); }; tmp; }));
}

void MessageModeldb5825___setRoles_roles_setList(void* ptr, int key, void* i)
{
	static_cast<QMap<qint32, QByteArray>*>(ptr)->insert(key, *static_cast<QByteArray*>(i));
}

void* MessageModeldb5825___setRoles_roles_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QMap<qint32, QByteArray>();
}

struct Moc_PackedList MessageModeldb5825___setRoles_roles_keyList(void* ptr)
{
	return ({ QList<qint32>* tmpValuecf92c1 = new QList<qint32>(static_cast<QMap<qint32, QByteArray>*>(ptr)->keys()); Moc_PackedList { tmpValuecf92c1, tmpValuecf92c1->size() }; });
}

void* MessageModeldb5825___rolesChanged_roles_atList(void* ptr, int v, int i)
{
	return new QByteArray(({ QByteArray tmp = static_cast<QMap<qint32, QByteArray>*>(ptr)->value(v); if (i == static_cast<QMap<qint32, QByteArray>*>(ptr)->size()-1) { static_cast<QMap<qint32, QByteArray>*>(ptr)->~QMap(); free(ptr); }; tmp; }));
}

void MessageModeldb5825___rolesChanged_roles_setList(void* ptr, int key, void* i)
{
	static_cast<QMap<qint32, QByteArray>*>(ptr)->insert(key, *static_cast<QByteArray*>(i));
}

void* MessageModeldb5825___rolesChanged_roles_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QMap<qint32, QByteArray>();
}

struct Moc_PackedList MessageModeldb5825___rolesChanged_roles_keyList(void* ptr)
{
	return ({ QList<qint32>* tmpValuecf92c1 = new QList<qint32>(static_cast<QMap<qint32, QByteArray>*>(ptr)->keys()); Moc_PackedList { tmpValuecf92c1, tmpValuecf92c1->size() }; });
}

int MessageModeldb5825_____roles_keyList_atList(void* ptr, int i)
{
	return ({qint32 tmp = static_cast<QList<qint32>*>(ptr)->at(i); if (i == static_cast<QList<qint32>*>(ptr)->size()-1) { static_cast<QList<qint32>*>(ptr)->~QList(); free(ptr); }; tmp; });
}

void MessageModeldb5825_____roles_keyList_setList(void* ptr, int i)
{
	static_cast<QList<qint32>*>(ptr)->append(i);
}

void* MessageModeldb5825_____roles_keyList_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<qint32>();
}

int MessageModeldb5825_____setRoles_roles_keyList_atList(void* ptr, int i)
{
	return ({qint32 tmp = static_cast<QList<qint32>*>(ptr)->at(i); if (i == static_cast<QList<qint32>*>(ptr)->size()-1) { static_cast<QList<qint32>*>(ptr)->~QList(); free(ptr); }; tmp; });
}

void MessageModeldb5825_____setRoles_roles_keyList_setList(void* ptr, int i)
{
	static_cast<QList<qint32>*>(ptr)->append(i);
}

void* MessageModeldb5825_____setRoles_roles_keyList_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<qint32>();
}

int MessageModeldb5825_____rolesChanged_roles_keyList_atList(void* ptr, int i)
{
	return ({qint32 tmp = static_cast<QList<qint32>*>(ptr)->at(i); if (i == static_cast<QList<qint32>*>(ptr)->size()-1) { static_cast<QList<qint32>*>(ptr)->~QList(); free(ptr); }; tmp; });
}

void MessageModeldb5825_____rolesChanged_roles_keyList_setList(void* ptr, int i)
{
	static_cast<QList<qint32>*>(ptr)->append(i);
}

void* MessageModeldb5825_____rolesChanged_roles_keyList_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<qint32>();
}

void* MessageModeldb5825_NewMessageModel(void* parent)
{
	if (dynamic_cast<QGraphicsObject*>(static_cast<QObject*>(parent))) {
		return new MessageModeldb5825(static_cast<QGraphicsObject*>(parent));
	} else if (dynamic_cast<QGraphicsWidget*>(static_cast<QObject*>(parent))) {
		return new MessageModeldb5825(static_cast<QGraphicsWidget*>(parent));
	} else if (dynamic_cast<QLayout*>(static_cast<QObject*>(parent))) {
		return new MessageModeldb5825(static_cast<QLayout*>(parent));
	} else if (dynamic_cast<QOffscreenSurface*>(static_cast<QObject*>(parent))) {
		return new MessageModeldb5825(static_cast<QOffscreenSurface*>(parent));
	} else if (dynamic_cast<QPaintDeviceWindow*>(static_cast<QObject*>(parent))) {
		return new MessageModeldb5825(static_cast<QPaintDeviceWindow*>(parent));
	} else if (dynamic_cast<QPdfWriter*>(static_cast<QObject*>(parent))) {
		return new MessageModeldb5825(static_cast<QPdfWriter*>(parent));
	} else if (dynamic_cast<QQuickItem*>(static_cast<QObject*>(parent))) {
		return new MessageModeldb5825(static_cast<QQuickItem*>(parent));
	} else if (dynamic_cast<QWidget*>(static_cast<QObject*>(parent))) {
		return new MessageModeldb5825(static_cast<QWidget*>(parent));
	} else if (dynamic_cast<QWindow*>(static_cast<QObject*>(parent))) {
		return new MessageModeldb5825(static_cast<QWindow*>(parent));
	} else {
		return new MessageModeldb5825(static_cast<QObject*>(parent));
	}
}

void MessageModeldb5825_DestroyMessageModel(void* ptr)
{
	static_cast<MessageModeldb5825*>(ptr)->~MessageModeldb5825();
}

char MessageModeldb5825_DropMimeDataDefault(void* ptr, void* data, long long action, int row, int column, void* parent)
{
	return static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::dropMimeData(static_cast<QMimeData*>(data), static_cast<Qt::DropAction>(action), row, column, *static_cast<QModelIndex*>(parent));
}

void* MessageModeldb5825_IndexDefault(void* ptr, int row, int column, void* parent)
{
	return new QModelIndex(static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::index(row, column, *static_cast<QModelIndex*>(parent)));
}

void* MessageModeldb5825_SiblingDefault(void* ptr, int row, int column, void* idx)
{
	return new QModelIndex(static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::sibling(row, column, *static_cast<QModelIndex*>(idx)));
}

long long MessageModeldb5825_FlagsDefault(void* ptr, void* index)
{
	return static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::flags(*static_cast<QModelIndex*>(index));
}

char MessageModeldb5825_InsertColumnsDefault(void* ptr, int column, int count, void* parent)
{
	return static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::insertColumns(column, count, *static_cast<QModelIndex*>(parent));
}

char MessageModeldb5825_InsertRowsDefault(void* ptr, int row, int count, void* parent)
{
	return static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::insertRows(row, count, *static_cast<QModelIndex*>(parent));
}

char MessageModeldb5825_MoveColumnsDefault(void* ptr, void* sourceParent, int sourceColumn, int count, void* destinationParent, int destinationChild)
{
	return static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::moveColumns(*static_cast<QModelIndex*>(sourceParent), sourceColumn, count, *static_cast<QModelIndex*>(destinationParent), destinationChild);
}

char MessageModeldb5825_MoveRowsDefault(void* ptr, void* sourceParent, int sourceRow, int count, void* destinationParent, int destinationChild)
{
	return static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::moveRows(*static_cast<QModelIndex*>(sourceParent), sourceRow, count, *static_cast<QModelIndex*>(destinationParent), destinationChild);
}

char MessageModeldb5825_RemoveColumnsDefault(void* ptr, int column, int count, void* parent)
{
	return static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::removeColumns(column, count, *static_cast<QModelIndex*>(parent));
}

char MessageModeldb5825_RemoveRowsDefault(void* ptr, int row, int count, void* parent)
{
	return static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::removeRows(row, count, *static_cast<QModelIndex*>(parent));
}

char MessageModeldb5825_SetDataDefault(void* ptr, void* index, void* value, int role)
{
	return static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::setData(*static_cast<QModelIndex*>(index), *static_cast<QVariant*>(value), role);
}

char MessageModeldb5825_SetHeaderDataDefault(void* ptr, int section, long long orientation, void* value, int role)
{
	return static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::setHeaderData(section, static_cast<Qt::Orientation>(orientation), *static_cast<QVariant*>(value), role);
}

char MessageModeldb5825_SetItemDataDefault(void* ptr, void* index, void* roles)
{
	return static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::setItemData(*static_cast<QModelIndex*>(index), *static_cast<QMap<int, QVariant>*>(roles));
}

char MessageModeldb5825_SubmitDefault(void* ptr)
{
	return static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::submit();
}

void MessageModeldb5825_FetchMoreDefault(void* ptr, void* parent)
{
	static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::fetchMore(*static_cast<QModelIndex*>(parent));
}

void MessageModeldb5825_ResetInternalDataDefault(void* ptr)
{
	static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::resetInternalData();
}

void MessageModeldb5825_RevertDefault(void* ptr)
{
	static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::revert();
}

void MessageModeldb5825_SortDefault(void* ptr, int column, long long order)
{
	static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::sort(column, static_cast<Qt::SortOrder>(order));
}

struct Moc_PackedList MessageModeldb5825_RoleNamesDefault(void* ptr)
{
	return ({ QHash<int, QByteArray>* tmpValuee210ed = new QHash<int, QByteArray>(static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::roleNames()); Moc_PackedList { tmpValuee210ed, tmpValuee210ed->size() }; });
}

struct Moc_PackedList MessageModeldb5825_ItemDataDefault(void* ptr, void* index)
{
	return ({ QMap<int, QVariant>* tmpValue4d6956 = new QMap<int, QVariant>(static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::itemData(*static_cast<QModelIndex*>(index))); Moc_PackedList { tmpValue4d6956, tmpValue4d6956->size() }; });
}

void* MessageModeldb5825_MimeDataDefault(void* ptr, void* indexes)
{
	return static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::mimeData(({ QList<QModelIndex>* tmpP = static_cast<QList<QModelIndex>*>(indexes); QList<QModelIndex> tmpV = *tmpP; tmpP->~QList(); free(tmpP); tmpV; }));
}

void* MessageModeldb5825_BuddyDefault(void* ptr, void* index)
{
	return new QModelIndex(static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::buddy(*static_cast<QModelIndex*>(index)));
}

void* MessageModeldb5825_ParentDefault(void* ptr, void* index)
{
	return new QModelIndex(static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::parent(*static_cast<QModelIndex*>(index)));
}

struct Moc_PackedList MessageModeldb5825_MatchDefault(void* ptr, void* start, int role, void* value, int hits, long long flags)
{
	return ({ QList<QModelIndex>* tmpValue8ce646 = new QList<QModelIndex>(static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::match(*static_cast<QModelIndex*>(start), role, *static_cast<QVariant*>(value), hits, static_cast<Qt::MatchFlag>(flags))); Moc_PackedList { tmpValue8ce646, tmpValue8ce646->size() }; });
}

void* MessageModeldb5825_SpanDefault(void* ptr, void* index)
{
	return ({ QSize tmpValue = static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::span(*static_cast<QModelIndex*>(index)); new QSize(tmpValue.width(), tmpValue.height()); });
}

struct Moc_PackedString MessageModeldb5825_MimeTypesDefault(void* ptr)
{
	return ({ QByteArray* t3d53af = new QByteArray(static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::mimeTypes().join("¡¦!").toUtf8()); Moc_PackedString { const_cast<char*>(t3d53af->prepend("WHITESPACE").constData()+10), t3d53af->size()-10, t3d53af }; });
}

void* MessageModeldb5825_DataDefault(void* ptr, void* index, int role)
{
	Q_UNUSED(ptr);
	Q_UNUSED(index);
	Q_UNUSED(role);

}

void* MessageModeldb5825_HeaderDataDefault(void* ptr, int section, long long orientation, int role)
{
	return new QVariant(static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::headerData(section, static_cast<Qt::Orientation>(orientation), role));
}

long long MessageModeldb5825_SupportedDragActionsDefault(void* ptr)
{
	return static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::supportedDragActions();
}

long long MessageModeldb5825_SupportedDropActionsDefault(void* ptr)
{
	return static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::supportedDropActions();
}

char MessageModeldb5825_CanDropMimeDataDefault(void* ptr, void* data, long long action, int row, int column, void* parent)
{
	return static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::canDropMimeData(static_cast<QMimeData*>(data), static_cast<Qt::DropAction>(action), row, column, *static_cast<QModelIndex*>(parent));
}

char MessageModeldb5825_CanFetchMoreDefault(void* ptr, void* parent)
{
	return static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::canFetchMore(*static_cast<QModelIndex*>(parent));
}

char MessageModeldb5825_HasChildrenDefault(void* ptr, void* parent)
{
	return static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::hasChildren(*static_cast<QModelIndex*>(parent));
}

int MessageModeldb5825_ColumnCountDefault(void* ptr, void* parent)
{
	return static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::columnCount(*static_cast<QModelIndex*>(parent));
}

int MessageModeldb5825_RowCountDefault(void* ptr, void* parent)
{
	Q_UNUSED(ptr);
	Q_UNUSED(parent);

}

char MessageModeldb5825_EventDefault(void* ptr, void* e)
{
	return static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::event(static_cast<QEvent*>(e));
}

char MessageModeldb5825_EventFilterDefault(void* ptr, void* watched, void* event)
{
	return static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::eventFilter(static_cast<QObject*>(watched), static_cast<QEvent*>(event));
}

void MessageModeldb5825_ChildEventDefault(void* ptr, void* event)
{
	static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::childEvent(static_cast<QChildEvent*>(event));
}

void MessageModeldb5825_ConnectNotifyDefault(void* ptr, void* sign)
{
	static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::connectNotify(*static_cast<QMetaMethod*>(sign));
}

void MessageModeldb5825_CustomEventDefault(void* ptr, void* event)
{
	static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::customEvent(static_cast<QEvent*>(event));
}

void MessageModeldb5825_DeleteLaterDefault(void* ptr)
{
	static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::deleteLater();
}

void MessageModeldb5825_DisconnectNotifyDefault(void* ptr, void* sign)
{
	static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::disconnectNotify(*static_cast<QMetaMethod*>(sign));
}

void MessageModeldb5825_TimerEventDefault(void* ptr, void* event)
{
	static_cast<MessageModeldb5825*>(ptr)->QAbstractListModel::timerEvent(static_cast<QTimerEvent*>(event));
}



void TestStructdb5825_Send(void* ptr, struct Moc_PackedString v0)
{
	QMetaObject::invokeMethod(static_cast<TestStructdb5825*>(ptr), "send", Q_ARG(QString, QString::fromUtf8(v0.data, v0.len)));
}

void* TestStructdb5825_Messages(void* ptr)
{
	return static_cast<TestStructdb5825*>(ptr)->messages();
}

void* TestStructdb5825_MessagesDefault(void* ptr)
{
	return static_cast<TestStructdb5825*>(ptr)->messagesDefault();
}

void TestStructdb5825_SetMessages(void* ptr, void* messages)
{
	static_cast<TestStructdb5825*>(ptr)->setMessages(static_cast<MessageModeldb5825*>(messages));
}

void TestStructdb5825_SetMessagesDefault(void* ptr, void* messages)
{
	static_cast<TestStructdb5825*>(ptr)->setMessagesDefault(static_cast<MessageModeldb5825*>(messages));
}

void TestStructdb5825_ConnectMessagesChanged(void* ptr, long long t)
{
	QObject::connect(static_cast<TestStructdb5825*>(ptr), static_cast<void (TestStructdb5825::*)(MessageModeldb5825*)>(&TestStructdb5825::messagesChanged), static_cast<TestStructdb5825*>(ptr), static_cast<void (TestStructdb5825::*)(MessageModeldb5825*)>(&TestStructdb5825::Signal_MessagesChanged), static_cast<Qt::ConnectionType>(t));
}

void TestStructdb5825_DisconnectMessagesChanged(void* ptr)
{
	QObject::disconnect(static_cast<TestStructdb5825*>(ptr), static_cast<void (TestStructdb5825::*)(MessageModeldb5825*)>(&TestStructdb5825::messagesChanged), static_cast<TestStructdb5825*>(ptr), static_cast<void (TestStructdb5825::*)(MessageModeldb5825*)>(&TestStructdb5825::Signal_MessagesChanged));
}

void TestStructdb5825_MessagesChanged(void* ptr, void* messages)
{
	static_cast<TestStructdb5825*>(ptr)->messagesChanged(static_cast<MessageModeldb5825*>(messages));
}

int TestStructdb5825_TestStructdb5825_QRegisterMetaType()
{
	return qRegisterMetaType<TestStructdb5825*>();
}

int TestStructdb5825_TestStructdb5825_QRegisterMetaType2(char* typeName)
{
	return qRegisterMetaType<TestStructdb5825*>(const_cast<const char*>(typeName));
}

int TestStructdb5825_TestStructdb5825_QmlRegisterType()
{
#ifdef QT_QML_LIB
	return qmlRegisterType<TestStructdb5825>();
#else
	return 0;
#endif
}

int TestStructdb5825_TestStructdb5825_QmlRegisterType2(char* uri, int versionMajor, int versionMinor, char* qmlName)
{
#ifdef QT_QML_LIB
	return qmlRegisterType<TestStructdb5825>(const_cast<const char*>(uri), versionMajor, versionMinor, const_cast<const char*>(qmlName));
#else
	return 0;
#endif
}

int TestStructdb5825_TestStructdb5825_QmlRegisterUncreatableType(char* uri, int versionMajor, int versionMinor, char* qmlName, struct Moc_PackedString message)
{
#ifdef QT_QML_LIB
	return qmlRegisterUncreatableType<TestStructdb5825>(const_cast<const char*>(uri), versionMajor, versionMinor, const_cast<const char*>(qmlName), QString::fromUtf8(message.data, message.len));
#else
	return 0;
#endif
}

void* TestStructdb5825___dynamicPropertyNames_atList(void* ptr, int i)
{
	return new QByteArray(({QByteArray tmp = static_cast<QList<QByteArray>*>(ptr)->at(i); if (i == static_cast<QList<QByteArray>*>(ptr)->size()-1) { static_cast<QList<QByteArray>*>(ptr)->~QList(); free(ptr); }; tmp; }));
}

void TestStructdb5825___dynamicPropertyNames_setList(void* ptr, void* i)
{
	static_cast<QList<QByteArray>*>(ptr)->append(*static_cast<QByteArray*>(i));
}

void* TestStructdb5825___dynamicPropertyNames_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QByteArray>();
}

void* TestStructdb5825___findChildren_atList2(void* ptr, int i)
{
	return ({QObject* tmp = static_cast<QList<QObject*>*>(ptr)->at(i); if (i == static_cast<QList<QObject*>*>(ptr)->size()-1) { static_cast<QList<QObject*>*>(ptr)->~QList(); free(ptr); }; tmp; });
}

void TestStructdb5825___findChildren_setList2(void* ptr, void* i)
{
	static_cast<QList<QObject*>*>(ptr)->append(static_cast<QObject*>(i));
}

void* TestStructdb5825___findChildren_newList2(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QObject*>();
}

void* TestStructdb5825___findChildren_atList3(void* ptr, int i)
{
	return ({QObject* tmp = static_cast<QList<QObject*>*>(ptr)->at(i); if (i == static_cast<QList<QObject*>*>(ptr)->size()-1) { static_cast<QList<QObject*>*>(ptr)->~QList(); free(ptr); }; tmp; });
}

void TestStructdb5825___findChildren_setList3(void* ptr, void* i)
{
	static_cast<QList<QObject*>*>(ptr)->append(static_cast<QObject*>(i));
}

void* TestStructdb5825___findChildren_newList3(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QObject*>();
}

void* TestStructdb5825___findChildren_atList(void* ptr, int i)
{
	return ({QObject* tmp = static_cast<QList<QObject*>*>(ptr)->at(i); if (i == static_cast<QList<QObject*>*>(ptr)->size()-1) { static_cast<QList<QObject*>*>(ptr)->~QList(); free(ptr); }; tmp; });
}

void TestStructdb5825___findChildren_setList(void* ptr, void* i)
{
	static_cast<QList<QObject*>*>(ptr)->append(static_cast<QObject*>(i));
}

void* TestStructdb5825___findChildren_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QObject*>();
}

void* TestStructdb5825___children_atList(void* ptr, int i)
{
	return ({QObject * tmp = static_cast<QList<QObject *>*>(ptr)->at(i); if (i == static_cast<QList<QObject *>*>(ptr)->size()-1) { static_cast<QList<QObject *>*>(ptr)->~QList(); free(ptr); }; tmp; });
}

void TestStructdb5825___children_setList(void* ptr, void* i)
{
	static_cast<QList<QObject *>*>(ptr)->append(static_cast<QObject*>(i));
}

void* TestStructdb5825___children_newList(void* ptr)
{
	Q_UNUSED(ptr);
	return new QList<QObject *>();
}

void* TestStructdb5825_NewTestStruct(void* parent)
{
	if (dynamic_cast<QGraphicsObject*>(static_cast<QObject*>(parent))) {
		return new TestStructdb5825(static_cast<QGraphicsObject*>(parent));
	} else if (dynamic_cast<QGraphicsWidget*>(static_cast<QObject*>(parent))) {
		return new TestStructdb5825(static_cast<QGraphicsWidget*>(parent));
	} else if (dynamic_cast<QLayout*>(static_cast<QObject*>(parent))) {
		return new TestStructdb5825(static_cast<QLayout*>(parent));
	} else if (dynamic_cast<QOffscreenSurface*>(static_cast<QObject*>(parent))) {
		return new TestStructdb5825(static_cast<QOffscreenSurface*>(parent));
	} else if (dynamic_cast<QPaintDeviceWindow*>(static_cast<QObject*>(parent))) {
		return new TestStructdb5825(static_cast<QPaintDeviceWindow*>(parent));
	} else if (dynamic_cast<QPdfWriter*>(static_cast<QObject*>(parent))) {
		return new TestStructdb5825(static_cast<QPdfWriter*>(parent));
	} else if (dynamic_cast<QQuickItem*>(static_cast<QObject*>(parent))) {
		return new TestStructdb5825(static_cast<QQuickItem*>(parent));
	} else if (dynamic_cast<QWidget*>(static_cast<QObject*>(parent))) {
		return new TestStructdb5825(static_cast<QWidget*>(parent));
	} else if (dynamic_cast<QWindow*>(static_cast<QObject*>(parent))) {
		return new TestStructdb5825(static_cast<QWindow*>(parent));
	} else {
		return new TestStructdb5825(static_cast<QObject*>(parent));
	}
}

void TestStructdb5825_DestroyTestStruct(void* ptr)
{
	static_cast<TestStructdb5825*>(ptr)->~TestStructdb5825();
}

void TestStructdb5825_DestroyTestStructDefault(void* ptr)
{
	Q_UNUSED(ptr);

}

char TestStructdb5825_EventDefault(void* ptr, void* e)
{
	return static_cast<TestStructdb5825*>(ptr)->QObject::event(static_cast<QEvent*>(e));
}

char TestStructdb5825_EventFilterDefault(void* ptr, void* watched, void* event)
{
	return static_cast<TestStructdb5825*>(ptr)->QObject::eventFilter(static_cast<QObject*>(watched), static_cast<QEvent*>(event));
}

void TestStructdb5825_ChildEventDefault(void* ptr, void* event)
{
	static_cast<TestStructdb5825*>(ptr)->QObject::childEvent(static_cast<QChildEvent*>(event));
}

void TestStructdb5825_ConnectNotifyDefault(void* ptr, void* sign)
{
	static_cast<TestStructdb5825*>(ptr)->QObject::connectNotify(*static_cast<QMetaMethod*>(sign));
}

void TestStructdb5825_CustomEventDefault(void* ptr, void* event)
{
	static_cast<TestStructdb5825*>(ptr)->QObject::customEvent(static_cast<QEvent*>(event));
}

void TestStructdb5825_DeleteLaterDefault(void* ptr)
{
	static_cast<TestStructdb5825*>(ptr)->QObject::deleteLater();
}

void TestStructdb5825_DisconnectNotifyDefault(void* ptr, void* sign)
{
	static_cast<TestStructdb5825*>(ptr)->QObject::disconnectNotify(*static_cast<QMetaMethod*>(sign));
}

void TestStructdb5825_TimerEventDefault(void* ptr, void* event)
{
	static_cast<TestStructdb5825*>(ptr)->QObject::timerEvent(static_cast<QTimerEvent*>(event));
}



#include "moc_moc.h"
