# BarkBark

Multicast UDP chat.

# Installing QT deploy
```
export GO111MODULE=off; go get -v github.com/therecipe/qt/cmd/... && $(go env GOPATH)/bin/qtdeploy test desktop github.com/therecipe/examples/basic/widgets
```

# Building for UBports
```
export GO111MODULE=off
$(go env GOPATH)/bin/qtdeploy -docker build ubports_arm_xenial
```

## License

Copyright (C) 2020  Nikki Gaudreau

Licensed under the MIT license
