import QtQuick 2.7
import QtQuick.Window 2.0

Item {
    anchors {
        bottom: parent.bottom
        left: parent.left
        right: parent.right
    }
    height: if (Qt.inputMethod.visible) {
            return Qt.inputMethod.keyboardRectangle.height / Screen.devicePixelRatio;
        } else {
            return 0;
        }
}
