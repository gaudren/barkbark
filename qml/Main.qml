import QtQuick 2.7
import Ubuntu.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import QtGraphicalEffects 1.0

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'barkbark.barkbark'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    Page {
        anchors.fill: parent

        header: PageHeader {
            id: header
            title: i18n.tr('BarkBark')
        }

        Rectangle {
            anchors.fill: parent
            color: 'aliceblue'
        }

        Rectangle {
            anchors {
                top: header.bottom
                bottom: messageRow.top
                left: parent.left
                right: parent.right
                margins: 10
            }
            color: 'white'
            radius: 20
            clip: true
            id: chatRect

            Item {
                id: listItem
                anchors.fill: parent
                layer.enabled: true
                layer.effect: OpacityMask {
                    maskSource: Item {
                        width: listItem.width
                        height: listItem.height
                        Rectangle {
                            anchors.fill: parent
                            radius: chatRect.radius
                        }
                    }
                }

                ListView {
                    id: listView
                    clip: true
                    model: testvar.messages

                    anchors {
                        fill: parent
                        margins: 5
                    }

                    delegate: Label {
                        text: 'test value ' + model.text
                        horizontalAlignment: Label.AlignHCenter
                        Layout.fillWidth: true
                        textFormat: Text.PlainText
                    }

                    onCountChanged: listView.positionViewAtEnd()
                }

            }
        }
        RowLayout {
            id: messageRow
            anchors {
                bottom: keyItem.top
                left: parent.left
                right: parent.right
                leftMargin: 10
                rightMargin: 10
            }

            TextArea {
                anchors.bottom: parent.bottom
                id: messageText
                wrapMode: Text.Wrap
                placeholderText: "Message"
                Layout.fillWidth: true
                Layout.maximumHeight: (root.height - keyItem.height) / 2
                autoSize: true
            }

            Button {
                color: 'aquamarine'
                Layout.preferredWidth: height * 2
                Layout.fillHeight: true
                onClicked: {
                    testvar.send(messageText.text);
                    messageText.text = '';
                    messageText.focus = true;
                }
                Layout.alignment: Qt.AlignCenter

                Icon {
                    name: 'send'
                    width: parent.height * 0.8
                    height: parent.height * 0.8

                    anchors {
                        verticalCenter: parent.verticalCenter
                        horizontalCenter: parent.horizontalCenter
                    }
                }
            }
        }
        KeyItem {
            id: keyItem
        }
    }
}
